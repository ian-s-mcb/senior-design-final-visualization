import json
import pandas as pd

from utils import boro_cd_dict

# Load data
shelter_df = pd.read_csv('./data/shelters_by_cd/shelter_census_by_cd.csv', index_col='Report Date')
with open('./data/shelters_by_cd/cds.geojson', 'rb') as f:
    cds_geojson = json.load(f)

# Parse date index
shelter_df.index = pd.to_datetime(shelter_df.index).values.astype('datetime64[M]')
shelter_df.index = shelter_df.index.rename('Date')

# Replace NaN with zeros
shelter_df.fillna(0, inplace=True)

# Add CD code column
concat_boro_cd = lambda row: f"{boro_cd_dict[row['Borough']]}{row['Community Districts']:02}"
shelter_df['CD'] = shelter_df[['Borough', 'Community Districts']].apply(concat_boro_cd, axis=1)

# Add total column
shelter_df['Shelter Population'] = shelter_df[[
    'Adult Family Shelter',
    'Adult Shelter',
    'Family Cluster',
    'Family with Children Commercial Hotel',
    'Family with Children Shelter',
    'Adult Shelter Commercial Hotel',
    'Adult Family Commercial Hotel',
]].sum(axis=1).astype(int)

# Drop unwanted columns
shelter_df = shelter_df[['Shelter Population', 'CD']]

# Sort by date
shelter_df.sort_index(inplace=True)

# Prepare inputs for date slider
start_date = '2019-01'
end_date = shelter_df.index[-1]
months = pd.date_range(start=start_date, end=end_date, freq='MS').strftime('%b %Y').to_list()
num_of_months = len(months) - 1
abbreviation_frequency = 6
abbreviated_months = pd.date_range(start=start_date, end=end_date, freq=f'{abbreviation_frequency}MS')
abbreviated_months = abbreviated_months.strftime('%b %Y').to_list()
