import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.manifold import TSNE

from utils import cd_boro_dict, cd_to_name, cd_to_borough
from apps.general_demographics.constants import demographics_categories

# Load dataset
df = pd.read_csv(
    'data/general_demographics/nyc_demographics_2010_by_cd-preprocessed.csv',
    index_col='cd',
)
df.index = df.index.astype(str)

# Scale data
scaler = StandardScaler()
df_scaled = df.copy()
df_scaled[df_scaled.columns] = scaler.fit_transform(df_scaled[df_scaled.columns])

# Group labels
cd_nums = df.index.to_list()
cd_names = list(map(cd_to_name, cd_nums))
classes = list(map(cd_to_borough, cd_nums))
classes_unique = list(cd_boro_dict.values())

# Group labels
cd_nums = df.index.to_list()
cd_names = list(map(cd_to_name, cd_nums))
classes = list(map(cd_to_borough, cd_nums))
classes_unique = list(cd_boro_dict.values())

def run_tsne(perplexity):
    n_components = 2
    RS = 123

    tsne_results = TSNE(
        random_state=RS,
        n_components=n_components,
        perplexity=perplexity,
    ).fit_transform(df_scaled)
    result = pd.DataFrame(
        data={
            'x': tsne_results[:, 0],
            'y': tsne_results[:, 1],
            'class': classes,
            'cd_name': cd_names,
            'cd_num': cd_nums,
        }
    )

    return result

def scale_df(selected_columns, selected_rows):
    # Filter down to selected columns and rows
    df_scaled = df.copy()[selected_columns].loc[selected_rows]

    # Scale feature vals from 0 to 1
    df_scaled[selected_columns] = df_scaled.div(df_scaled.sum(axis=1), axis=0).mul(100)
    df_scaled['cd_name'] = list(map(cd_to_name, df_scaled.index.to_list()))

    # Keep only one decimal place
    df_scaled = df_scaled.round(1)

    return df_scaled