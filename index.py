from dash import dcc
from dash import html
from dash.dependencies import Input, Output

from app import app, server
from apps.general_demographics import general_demographics
from apps.shelters_by_cd import shelters_by_cd
import credits
import navbar

app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    navbar.layout,
    html.Div(id='page-content', className='container')
])

@app.callback(
    Output('page-content', 'children'),
    Input('url', 'pathname')
)
def display_page(pathname):
    if pathname == '/':
        return general_demographics.layout
    elif pathname == '/shelters':
        return shelters_by_cd.layout
        return app2.layout
    elif pathname == '/credits':
        return credits.layout
    else:
        return '404'

if __name__ == '__main__':
    app.run_server(debug=True)
