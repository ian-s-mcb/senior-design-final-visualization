import dash_bootstrap_components as dbc
from dash import dcc
from dash import html
from dash.dependencies import Input, Output

from app import app
from apps.general_demographics.general_scatter import scatter_legend_html  # For helper and side-effect
from apps.general_demographics import general_bar # Side-effect only
from apps.general_demographics.constants import demographics_categories, demographics_plot_titles

note_about_data = html.Div([
    html.B('Note about data:'),
    html.Div([
        'These plots are based on US 2010 Census dataset, which you can read about on the ',
        html.A('credits page', href='/credits'),
        '.',
    ]),
])

layout = html.Div([
    dbc.Row([
        dbc.Col([
            html.Div('General demographics scatter (2-component t-SNE)', className='h4'),
            dcc.Graph(id='genDemogScatter'),
            html.Div(id='genDemogBarTitle', className='h4'),
            dcc.Graph(id='genDemogBar'),
        ],
            sm=9,
            md=9,
        ),
        dbc.Col([
            scatter_legend_html,
            html.B(id='genDemogScatterSliderTitle'),
            dcc.Slider(
                id='genDemogScatterSlider',
                marks={'5': '5', '50': '50'},
                min=5,
                max=50,
                value=30,
                step=1,
            ),
            html.B('Demographic category'),
            dcc.Dropdown(
                id='genDemogBarDropdown',
                options=[{'label': i, 'value': i} for i in demographics_categories.keys()],
                value='age',
            ),
            html.Div(id='genDemogBarLegend'),
            note_about_data,
        ],
            sm=3,
            md=3,
        )
    ])
])

@app.callback(
    Output('genDemogBarTitle', 'children'),
    Input('genDemogBarDropdown', 'value'),
)
def display_bar_title(feature_category):
    return demographics_plot_titles[feature_category]

@app.callback(
    Output('genDemogScatterSliderTitle', 'children'),
    Input('genDemogScatterSlider', 'value'),
)
def display_perplexity(value):
    return f'Current t-SNE perplexity: {value}'
