import dash_bootstrap_components as dbc
from dash import html
from dash.dependencies import Input, Output, State

from app import app

PLOTLY_LOGO = 'https://images.plot.ly/logo/new-branding/plotly-logomark.png'

nav1 = dbc.Nav(
    id='nav1',
    pills=True,
)

nav2 = dbc.Nav(
    dbc.DropdownMenu(
        [
            dbc.DropdownMenuItem(dbc.NavLink('Credits', href='/credits')),
            dbc.DropdownMenuItem(dbc.NavLink('Source code', href='https://github.com/ian-s-mcb/senior-design-final-visualization')),
            dbc.DropdownMenuItem(dbc.NavLink('Journal paper', href='#')),
        ],
        label='About',
        nav=True,
        right=True,
    ),
)

layout = dbc.Navbar(
    [
        # Logo/brand
        html.A(
            dbc.Row(
                [
                    dbc.Col(html.Img(src=PLOTLY_LOGO, height='30px')),
                    dbc.Col(dbc.NavbarBrand('nyc-homeless-covid-impact', className='ml-2', style={'fontSize': '16px'})),
                ],
                align='center',
                className="g-0",
            ),
            href='/',
        ),
        dbc.NavbarToggler(id='navbarToggler'),
        dbc.Collapse(
            [
                nav1,
                nav2,
            ],
            id='navbarCollapse',
            navbar=True,
            className='justify-content-between'
        ),
    ],
    color='dark',
    dark=True,
    expand='lg',
)

@app.callback(
    Output('nav1', 'children'),
    Input('url', 'pathname')
)
def set_active_navlink(pathname):
    return [
        dbc.NavItem(dbc.NavLink('A student data vis project', disabled=True)),
        dbc.NavItem(dbc.NavLink('General population', active=(pathname == '/'), href='/')),
        dbc.NavItem(dbc.NavLink('Shelters', active=(pathname == '/shelters'), href='/shelters')),
    ]

# Callback for toggling the collapse on small screens
@app.callback(
    Output('navbarCollapse', 'is_open'),
    [Input('navbarToggler', 'n_clicks')],
    [State('navbarCollapse', 'is_open')],
)
def toggle_navbar_collapse(n, is_open):
    if n:
        return not is_open
    return is_open
