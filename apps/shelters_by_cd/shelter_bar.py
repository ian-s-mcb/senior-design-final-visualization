from dash.dependencies import Input, Output
import plotly.express as px

from app import app
from apps.shelters_by_cd.analysis import months, shelter_df
from utils import cd_to_name

@app.callback(
    Output('shelterBar', 'figure'),
    Output('shelterBarTitle', 'children'),
    Input('sheltersMap', 'clickData'),
    Input('sheltersMap', 'selectedData'),
    Input('shelterBarDateSlider', 'value'))
def create_shelter_bar(clickData, selectedData, dateRange):
    '''Create a bar figure for an one or more shelters'''
    start_date = months[dateRange[0]]
    end_date = months[dateRange[1]]
    date_str = f'({start_date} - {end_date})'

    if selectedData:
        cds = [d['location'] for d in selectedData['points']]
        title = f'Shelter population in selected CDs {date_str}'
    elif clickData:
        cds = [d['location'] for d in clickData['points']]
        title = f'Shelter population in CD-{cds[0]} {date_str}'
    else:
        cds = ['109']
        title = f'Shelter population in CD-{cds[0]} {date_str}'

    selection = shelter_df[shelter_df['CD'].isin(cds)][start_date:end_date].copy()
    selection['name'] = selection['CD'].apply(cd_to_name)

    fig = px.bar(
        selection,
        barmode='group',
        color='CD',
        hover_data=['Shelter Population'],
        hover_name='name',
        labels={
            'index': 'Month',
            'Shelter Population': 'Population',
        },
        x=selection.index,
        y='Shelter Population',
    )
    fig.update_layout(
        margin={'r': 0, 'b': 0, 'l': 0, 't': 0},
    )

    return fig, title