from dash import html
from dash.dependencies import Input, Output
import plotly.express as px

from app import app
from apps.general_demographics.analysis import classes_unique, run_tsne
from apps.general_demographics.constants import colors

# Build legend
scatter_legend_items = [
    html.Li([
        html.Span(
            '●',
            style={
                'color': colors[i],
                'fontSize': 20,
                'verticalAlign': 'middle',
            }
        ),
        html.Span(
            f' {class_name}',
            style={'verticalAlign': 'middle'}),
    ],
        style={
            'margin': '-10px 0 0',
        },
    )
    for i, class_name in enumerate(classes_unique)
]
scatter_legend_html = html.Div([
    html.B('Borough'),
    html.Ul(
        scatter_legend_items,
        style={'listStyle': 'none'},
        className='ml-3 p-0',
    )
])

@app.callback(
    Output('genDemogScatter', 'figure'),
    Input('genDemogScatterSlider', 'value')
)
def create_scatter(perplexity):
    fig = px.scatter(
        run_tsne(perplexity),
        color='class',
        custom_data=['cd_num'],
        labels={'class': 'Borough', 'x': 'X component', 'y': 'Y component'},
        hover_name='cd_name',
        hover_data={'class': False, 'x': ':.2f', 'y': ':.2f'},
        color_discrete_sequence=colors,
        x='x', y='y',
    )
    fig.update_layout(
        margin={'r': 0, 'b': 0, 'l': 0, 't': 0},
        showlegend=False,
        xaxis_tickangle=-45,
        yaxis=dict(scaleanchor = 'x')
    )

    return fig
