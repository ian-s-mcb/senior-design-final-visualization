from dash import dcc
from dash import html

credits = '''
## Credits

Analysis, visualization and web design by me, [ian-s-mcb][gl].

Credit is due to my instructor, [Dr. Ronak Etemadpour][prof], who guided my work and help me tease insights out of uncooperative data.

I am also indebted to the following websites, for the valuable datasets they made publicly available.

* NYC Open Data's [2000/2010 census demographics][demog-data]
    * Grouped at the NYC Community District (CD) level (a total of 59 regions)
    * Contains 55 features
* NYC Open Data's [2018-present NYC shelter census][shelter-by-cd-data]
    * Grouped at the CD level
    * Summed all resident types (including placement in shelters and commercial hotels)
    * Resident types include: adult family, adult, family cluster, and family with children
* NYC Open Data's [boundaries of community districts][cd-geojson-data]

[prof]: https://www.ccny.cuny.edu/profiles/ronak-etemadpour
[gl]: https://gitlab.com/ian-s-mcb
[demog-data]: https://data.cityofnewyork.us/City-Government/Census-Demographics-at-the-NYC-Community-District-/5unr-w4sc
[shelter-by-cd-data]: https://data.cityofnewyork.us/Social-Services/Individual-Census-by-Borough-Community-District-an/veav-vj3r
[cd-geojson-data]: https://data.cityofnewyork.us/City-Government/Community-Districts/yfnk-k7r4
'''

layout = html.Div(
    dcc.Markdown(credits)
)
