from dash import html
from dash.dependencies import Input, Output
import plotly.express as px

from app import app
from apps.shelters_by_cd.analysis import cds_geojson, months, shelter_df
from apps.shelters_by_cd.constants import nyc_coordinates
from utils import cd_to_name


@app.callback(
    Output('sheltersMap', 'figure'),
    Output('sheltersMapTitle', 'children'),
    Input('shelterMapDateSlider', 'value')
)
def create_shelters_map(month_index):
    """Create a choropleth map of NYC's CDs at one point in time,
    with the regions colored by the number of shelter residents"""
    date = months[month_index]
    selection = shelter_df.loc[date].copy()
    selection['name'] = selection['CD'].apply(cd_to_name)
    title = f'All shelters by Community District in {date}'

    fig = px.choropleth_mapbox(
        selection,
        center=nyc_coordinates,
        color='Shelter Population',
        color_continuous_scale='Viridis',
        featureidkey='properties.boro_cd',
        geojson=cds_geojson,
        hover_name='name',
        labels={
            'Shelter Population': 'Population',
        },
        locations='CD',
        mapbox_style='carto-positron',
        opacity=0.5,
        zoom=10,
    )
    
    fig.update_layout(
        margin={'r': 0, 'b': 0, 'l': 0, 't': 0},
    )

    return fig, title
