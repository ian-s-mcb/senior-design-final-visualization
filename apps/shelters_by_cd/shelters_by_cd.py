import dash_bootstrap_components as dbc
from dash import dcc
from dash import html
from dash.dependencies import Input, Output

from app import app
from apps.shelters_by_cd import shelter_bar, shelters_map # For side-effect only
from apps.shelters_by_cd.analysis import abbreviation_frequency, abbreviated_months, num_of_months, shelter_df

layout = html.Div([
    html.Div(
        className='h4',
        id='sheltersMapTitle',
    ),
    dcc.Graph(id='sheltersMap'),
    dcc.Slider(
        id='shelterMapDateSlider',
        min=0,
        max=num_of_months,
        marks={
            int(i * abbreviation_frequency): {
                'label': date,
                'style': {'transform': 'translateX(-10px) rotate(15deg)'},
            }
            for i, date in enumerate(abbreviated_months)},
        value=20 # Month: Sep 2020
    ),
    html.Div(
        className='h4',
        id='shelterBarTitle',
    ),
    dcc.Graph(id='shelterBar'),
    dcc.RangeSlider(
        id='shelterBarDateSlider',
        min=0,
        max=num_of_months,
        marks={
            int(i * abbreviation_frequency): {
                'label': date,
                'style': {'transform': 'translateX(-10px) rotate(15deg)'},
            }
            for i, date in enumerate(abbreviated_months)},
        value=[20, num_of_months] # Range: Sep 2020 - present
    ),
])
