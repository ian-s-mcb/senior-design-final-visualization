import plotly.express as px

colors = px.colors.qualitative.Plotly

# Names of columns for the different demographic categories
demographics_plot_titles = {
    'age': 'Age groups by CD',
    'housing_unit_size': 'Housing unit size by CD',
    'housing_renter_owner': 'Housing units rented or owned by CD',
    'persons_in_housing_type': 'Household type by CD',
    'race': 'Race by CD',
    'sex': 'Sex by CD',
}

demographics_categories = {
    'age': [
        'age-under-5-years',
        'age-5-to-9-years',
        'age-10-to-14-years',
        'age-15-to-19-years',
        'age-20-to-24-years',
        'age-25-to-44-years',
        'age-45-to-64-years',
        'age-65-years-and-over',
    ],
    'housing_unit_size': [
        'housing-unit-size-1-person',
        'housing-unit-size-2-person',
        'housing-unit-size-3-person',
        'housing-unit-size-4-person',
        'housing-unit-size-5-persons-and-over',
    ],
    'housing_renter_owner': [
        'housing-units-occupied-renter',
        'housing-units-occupied-owner',
    ],
    'persons_in_housing_type': [
        'persons-living-in-group-quarters',
        'persons-living-in-family-households',
        'persons-living-in-nonfamily-household',
    ],
    'race': [
        'race-white-nonhispanic',
        'race-black-nonhispanic',
        'race-asian-and-pacific-islander-nonhispanic',
        'race-other-nonhispanic',
        'race-two-or-more-races-nonhispanic',
        'race-hispanic-origin',
    ],
    'sex': [
        'sex-female',
        'sex-male',
    ],
}