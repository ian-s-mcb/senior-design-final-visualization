# senior-design-final-visualization

A data visualization of COVID-19 impact on NYC's homeless population for
the Senior Design course.

### [Demo][demo]

### Screencast

![Video screencast of visualization in action][screencast]

### Run locally

```bash
# Clone and cd into repo
pip install -r requirements.txt
python index.py
```

### Credits
Analysis, visualization and web design by me, [ian-s-mcb][profile].

Credit is due to my instructor, [Dr. Ronak Etemadpour][professor], who
guided my work and help me tease insights out of uncooperative data.


[demo]: https://nyc-homeless-covid-impact.herokuapp.com/
[professor]: https://www.ccny.cuny.edu/profiles/ronak-etemadpour
[profile]: https://gitlab.com/ian-s-mcb
[screencast]: https://i.imgur.com/2wa9qdp.mp4
