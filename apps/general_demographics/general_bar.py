from dash import html
from dash.dependencies import Input, Output
import plotly.express as px

from app import app
from apps.general_demographics.analysis import cd_nums, scale_df
from apps.general_demographics.constants import colors, demographics_categories, demographics_plot_titles


@app.callback(
    Output('genDemogBar', 'figure'),
    Input('genDemogScatter', 'selectedData'),
    Input('genDemogBarDropdown', 'value'),
)
def create_bar(selectedData, feature_category):
    # Identify what cds to plot
    if not selectedData:
        selected_cd_nums = cd_nums
    else:
        selected_cd_nums = [point['customdata'][0] for point in selectedData['points']]

    # Look up feature names
    feat_cols = demographics_categories[feature_category]
    # Scale data
    df_feat = scale_df(feat_cols, selected_cd_nums)

    # Plot
    fig = px.bar(
        df_feat,
        color_discrete_sequence=colors,
        x=df_feat.index,
        y=feat_cols,
        hover_name='cd_name',
        labels={
            'cd': 'Community District (CD)',
            'value': 'Percentage of population',
            'variable': 'Demographic',
        },
    )
    fig.update_layout(
        margin={'r': 0, 'b': 0, 'l': 0, 't': 0},
        showlegend=False,
        xaxis_tickangle=-45,
    )

    return fig

@app.callback(
    Output('genDemogBarLegend', 'children'),
    [Input('genDemogBarDropdown', 'value')])
def create_bar_legend(feature_category):
    features = demographics_categories[feature_category]
    legend_items = [
        html.Li([
            html.Span(
                '■',
                style={
                    'color': colors[i],
                    'fontSize': 20,
                    'verticalAlign': 'middle'
                }
            ),
            html.Span(
                f' {feature}',
                style={'verticalAlign': 'middle'}),
        ],
            style={'margin': '-10px 0 0'},
        )
        for i, feature in enumerate(features)
    ]
    legend_html = html.Div([
        html.B('Demographic'),
        html.Ul(
            legend_items,
            style={'listStyle': 'none'},
            className='ml-3 p-0',
        )
    ])
    return legend_html
